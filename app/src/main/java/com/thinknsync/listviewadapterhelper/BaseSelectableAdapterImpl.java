package com.thinknsync.listviewadapterhelper;

import android.content.Context;
import android.view.View;
import android.view.ViewGroup;

import java.util.ArrayList;
import java.util.List;
import java.util.Map;


public abstract class BaseSelectableAdapterImpl<T> extends BaseListViewAdapter<T> implements SelectableListAdapter<T> {

    protected int defaultColor = DEFAULT_COLOR;
    protected int selectedColor = SELECTED_COLOR;
    protected int errorColor = ERROR_COLOR;

    protected List<T> backgroundColorChangeElements;
    protected boolean shouldColor = true;
    protected boolean allowMultiSelect = true;

    public BaseSelectableAdapterImpl(Context context, List<T> objects) {
        super(context, objects);
        backgroundColorChangeElements = new ArrayList<>();
    }

    @Override
    public View getView(int position, View convertView, ViewGroup parent) {
        View view = super.getView(position, convertView, parent);
        if(shouldColor) {
            if (isColorChangeRequired(getItem(position))) {
                changeBackgroundColorOfItem(view, selectedColor);
            } else {
                changeBackgroundColorOfItem(view, defaultColor);
            }
        }

        return view;
    }

    @Override
    public void setShouldColor(boolean shouldColor) {
        this.shouldColor = shouldColor;
    }

    @Override
    public void setAllowMultiSelect(boolean allowMultiSelect) {
        this.allowMultiSelect = allowMultiSelect;
    }

    @Override
    public void setSelectedColor(int color) {
        selectedColor = color;
    }

    @Override
    public void setDefaultColor(int color) {
        defaultColor = color;
    }

    @Override
    public void setErrorColor(int color) {
        errorColor = color;
    }

    @Override
    public void changeBackgroundColorOfItem(View itemView, int color) {
        itemView.setBackgroundColor(color);
    }

    @Override
    public void setBackgroundColorChangeElements(List<T> changeBackgroundElements) {
        this.backgroundColorChangeElements = changeBackgroundElements;
        notifyDataSetChanged();
    }

    @Override
    public void addToBackgroundColorChangeElements(T changeBackgroundObject) {
        if(!allowMultiSelect){
            this.backgroundColorChangeElements.clear();
        }
        this.backgroundColorChangeElements.add(changeBackgroundObject);
        notifyDataSetChanged();
    }

    @Override
    public void removeFromBackgroundColorChangeElements(T changeBackgroundElements) {
        this.backgroundColorChangeElements.remove(changeBackgroundElements);
        notifyDataSetChanged();
    }
}
