package com.thinknsync.listviewadapterhelper.searchable;

import java.util.ArrayList;
import java.util.List;

public class SearchProcessImpl<T extends SearchableObject> implements SearchProcessContainer<T> {

    private List<T> searchableObjects;

    public SearchProcessImpl(List<T> searchableObjects){
        this.searchableObjects = searchableObjects;
    }

    @Override
    public List<T> search(String searchString) {
        List<T> filteredContents = new ArrayList<>();
        for (T searchableObject : searchableObjects) {
            for (String property : searchableObject.getSearchableProperties())
                if (!filteredContents.contains(searchableObject) && property.toLowerCase().contains(searchString.toLowerCase())) {
                    filteredContents.add(searchableObject);
                }
        }
        return filteredContents;
    }
}
