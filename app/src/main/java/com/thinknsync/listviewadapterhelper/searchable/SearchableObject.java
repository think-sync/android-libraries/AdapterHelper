package com.thinknsync.listviewadapterhelper.searchable;

public interface SearchableObject {
    String[] getSearchableProperties();
}
