package com.thinknsync.listviewadapterhelper.searchable;

import android.content.Context;
import android.widget.Filter;
import android.widget.Filterable;

import androidx.recyclerview.widget.RecyclerView;

import com.thinknsync.listviewadapterhelper.BaseListViewAdapter;
import com.thinknsync.listviewadapterhelper.recyclerView.BaseRecyclerViewAdapter;
import com.thinknsync.listviewadapterhelper.recyclerView.RecyclerViewHolder;

import java.util.List;

public abstract class BaseSearchableRecyclerViewAdapterImpl<T extends SearchableObject, U extends RecyclerView.ViewHolder & RecyclerViewHolder>
        extends BaseRecyclerViewAdapter<T, U> implements Filterable {

    private SearchProcessContainer<T> searchImplementation;
    private List<T> searchFilterResults;

    public BaseSearchableRecyclerViewAdapterImpl(Context context, List<T> objects, SearchProcessContainer<T> searchImplementation) {
        super(context, objects);
        this.searchFilterResults = objects;
        this.searchImplementation = searchImplementation;
    }

    @Override
    public int getItemCount() {
        return searchFilterResults.size();
    }

    @Override
    public void onBindViewHolder(U holder, final int position){
        setValues(holder, searchFilterResults.get(position));
    }

    @Override
    public Filter getFilter() {
        return new Filter() {
            @Override
            protected FilterResults performFiltering(CharSequence constraint) {
                FilterResults results = new FilterResults();

                if (constraint == null || constraint.length() == 0) {
                    results.values = objects;
                    results.count = objects.size();
                } else {
                    List<T> filteredContacts = searchImplementation.search(constraint.toString());
                    results.values = filteredContacts;
                    results.count = filteredContacts.size();
                }

                return results;
            }

            @Override
            protected void publishResults(CharSequence constraint, FilterResults results) {
                searchFilterResults = (List<T>)(List<?>) results.values;
                notifyDataSetChanged();
            }
        };
    }
}
