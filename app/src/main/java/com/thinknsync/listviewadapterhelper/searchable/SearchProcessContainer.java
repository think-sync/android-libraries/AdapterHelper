package com.thinknsync.listviewadapterhelper.searchable;

import java.util.List;

public interface SearchProcessContainer<T extends SearchableObject> {
    List<T> search(String searchString);
}