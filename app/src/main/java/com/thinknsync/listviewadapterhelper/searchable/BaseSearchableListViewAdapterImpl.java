package com.thinknsync.listviewadapterhelper.searchable;

import android.content.Context;
import android.widget.Filter;
import android.widget.Filterable;

import com.thinknsync.listviewadapterhelper.BaseListViewAdapter;

import java.util.ArrayList;
import java.util.List;

public abstract class BaseSearchableListViewAdapterImpl<T extends SearchableObject> extends BaseListViewAdapter<T> implements Filterable {

    private SearchProcessContainer<T> searchImplementation;
    private List<T> searchFilterResults;

    public BaseSearchableListViewAdapterImpl(Context context, List<T> objects, SearchProcessContainer<T> searchImplementation) {
        super(context, objects);
        this.searchFilterResults = objects;
        this.searchImplementation = searchImplementation;
    }

    @Override
    public int getCount() {
        return searchFilterResults.size();
    }

    @Override
    public T getItem(int position){
        return searchFilterResults.get(position);
    }

    @Override
    public Filter getFilter() {
        return new Filter() {
            @Override
            protected FilterResults performFiltering(CharSequence constraint) {
                FilterResults results = new FilterResults();

                if (constraint == null || constraint.length() == 0) {
                    results.values = objects;
                    results.count = objects.size();
                } else {
                    List<T> filteredContacts = searchImplementation.search(constraint.toString());
                    results.values = filteredContacts;
                    results.count = filteredContacts.size();
                }

                return results;
            }

            @Override
            protected void publishResults(CharSequence constraint, FilterResults results) {
                searchFilterResults = (List<T>)(List<?>) results.values;
                notifyDataSetChanged();
            }
        };
    }
}
