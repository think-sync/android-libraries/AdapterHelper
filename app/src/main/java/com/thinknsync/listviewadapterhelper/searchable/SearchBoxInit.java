package com.thinknsync.listviewadapterhelper.searchable;

import android.text.Editable;
import android.text.TextWatcher;
import android.widget.EditText;
import android.widget.Filterable;

public final class SearchBoxInit {

    public static void initSearchBox(EditText searchBox, final Filterable adapter){
        searchBox.addTextChangedListener(new TextWatcher() {
            @Override
            public void beforeTextChanged(CharSequence s, int start, int count, int after) {

            }

            @Override
            public void onTextChanged(CharSequence s, int start, int before, int count) {
                adapter.getFilter().filter(s);
            }

            @Override
            public void afterTextChanged(Editable s) {

            }
        });
    }
}
