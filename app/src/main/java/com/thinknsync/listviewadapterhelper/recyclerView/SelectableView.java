package com.thinknsync.listviewadapterhelper.recyclerView;

public interface SelectableView {
    void setItemSelected();
    void setItemUnSelected();
}
