package com.thinknsync.listviewadapterhelper.recyclerView;

public interface SelectableRecyclerViewHolder extends RecyclerViewHolder, SelectableView {
    void setItemSelected();
    void setItemUnSelected();
}
