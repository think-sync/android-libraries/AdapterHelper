package com.thinknsync.listviewadapterhelper;

import android.graphics.Color;
import android.view.View;

import java.util.List;

public interface SelectableListAdapter<T> {
    int SELECTED_COLOR = Color.GREEN;
    int DEFAULT_COLOR = Color.WHITE;
    int ERROR_COLOR = Color.RED;

    void setSelectedColor(int color);
    void setDefaultColor(int color);
    void setErrorColor(int color);
    void changeBackgroundColorOfItem(View itemView, int color);
    void setBackgroundColorChangeElements(List<T> changeBackgroundObjects);
    void addToBackgroundColorChangeElements(T changeBackgroundObject);
    void removeFromBackgroundColorChangeElements(T object);
    boolean isColorChangeRequired(T object);
    void setShouldColor(boolean shouldColor);
    void setAllowMultiSelect(boolean allowMultiSelect);
}
